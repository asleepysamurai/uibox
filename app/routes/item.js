
/*
 * All item pages
 */

var async = require('async');
var db = require('../utils/db.js');

exports.route = function(app){
	// item page
	app.get('/item/:id', function(req, res){
		db.select('items', ['items.id', 'items.name', 'items.long_desc', 'items.licenses', 'items.github', 'items.demo', 'items.author', 'items.downloads', 'items.likes', 'items.docs', 'items.bower', 'items.tags', 'items.deps'], ['items.id'], [req.params.id], function(err, result){
			if(err || !result || !result.rows || ! result.rows[0])
				return res.render('item', {title: 'Item', pageid: 'item', err: err});

			var item = result.rows[0];

			async.parallel({
				licenses: function(fn){
					db._query('SELECT licenses.name, licenses.short_name, licenses.link FROM licenses INNER JOIN item_licenses ON licenses.id = item_licenses.license_id WHERE item_licenses.item_id = $1', [item.id], fn);
				},
				deps: function(fn){
					db._query('SELECT deps.name FROM deps INNER JOIN item_deps ON deps.id = item_deps.dep_id WHERE item_deps.item_id = $1', [item.id], fn);
				},
				tags: function(fn){
					db._query('SELECT tags.id, tags.name FROM tags INNER JOIN item_tags ON tags.id = item_tags.tag_id WHERE item_tags.item_id = $1', [item.id], function(err, results){
						if(err || !results || !results.rows)
							return fn(err, results);
						else
							// Ignore tag with id = 3 (jquery) while searching for similar items
							db._query('SELECT DISTINCT items.id, items.name, items.short_desc, items.likes, items.downloads FROM items INNER JOIN item_tags ON items.id = item_tags.item_id WHERE item_tags.item_id <> $1 AND item_tags.tag_id IN ('+results.rows.map(function(r){ return r.id; }).filter(function(r){ return r != 3; }).join(',')+') AND random() < 0.55 LIMIT 4', [item.id], function(err, similar){
								item.similar = similar.rows;
								return fn(err, results);
							});
					});
				},
				author: function(fn){
					db.select('authors',['name','link'],['id'],[item.author],fn);
				}
			}, function(err, results){
				if(err)
					return res.render('item', {title: 'Item', pageid: 'item', err: err});

				item.author = results.author.rows[0];
				item.tags = results.tags.rows;
				item.deps = results.deps.rows;
				item.licenses = results.licenses.rows;

				return res.render('item', {title: item.name, pageid: 'item', err: err, item: item});
			});
		});
	});

	app.post('/item/:id/like', function(req, res){
		db._query('UPDATE items SET likes = likes+1 WHERE id = $1 RETURNING likes', [req.params.id], function(err, results){
			if(err || !results || !results.rows || !results.rows[0])
				return res.json({'error': true});
			return res.json({'likes': results.rows[0].likes});
		});
	});

	app.post('/item/:id/download', function(req, res){		
		db._query('UPDATE items SET downloads = downloads+1 WHERE id = $1 RETURNING downloads', [req.params.id], function(err, results){
			if(err || !results || !results.rows || !results.rows[0])
				return res.json({'error': true});
			return res.json({'downloads': results.rows[0].downloads});
		});
	});
};