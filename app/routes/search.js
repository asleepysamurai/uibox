
/*
 * Search and related routes
 */
var db = require('../utils/db.js');

var search_fields = 'items.id, items.name, items.short_desc, items.likes, items.downloads';

var search = function(req, res){
	var q = req.param('q');
	if(!q)
		return res.render('search', {title: 'Search for '+req.param('q'), pageid: 'search', query: req.param('q'), sort: req.param('s')});

	q = q.replace(/\s+/,' %s ');
	
	var s = req.param('s'), query;
	if(s == 'likes')
		query = 'SELECT '+search_fields+' FROM items WHERE search_tsv @@ plainto_tsquery($1) ORDER BY likes DESC';
	else if(s == 'downloads')
		query = 'SELECT '+search_fields+' FROM items WHERE search_tsv @@ plainto_tsquery($1) ORDER BY downloads DESC';
	else
		query = 'SELECT '+search_fields+', ts_rank_cd(search_tsv, plainto_tsquery($1)) AS rank FROM items WHERE search_tsv @@ plainto_tsquery($1) ORDER BY rank DESC';
	
	db._query(query, [q], function(err, results){
		if(err)
			return res.render('search', {title: 'Search for '+req.param('q'), pageid: 'search', query: req.param('q'), sort: req.param('s'), error: err});
		return res.render('search', {title: 'Search for '+req.param('q'), pageid: 'search', query: req.param('q'), sort: req.param('s'), res: results.rows});
	});
};

exports.route = function(app){
	// Search page
	app.get('/search', search);
	app.post('/search', search);

	// Tags page
	app.get('/tag/:name', function(req, res){
		var sortby = 'likes';
		if(req.query.s == 'downloads')
			sortby = 'downloads';

		db._query('SELECT '+search_fields+' FROM tags INNER JOIN item_tags ON tags.id = item_tags.tag_id INNER JOIN items ON item_tags.item_id = items.id WHERE tags.name=$1 ORDER BY '+sortby+' DESC',[req.params.name],function(err, results){
			if(err)
				return res.render('tag', {title: 'Tag: '+req.params.name, pageid: 'search', tag: req.params.name, sort: req.param('s'), error: err});
			return res.render('tag', {title: 'Tag: '+req.params.name, pageid: 'search', tag: req.params.name, sort: req.param('s'), res: results.rows});
		});
	});
};