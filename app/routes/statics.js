
/*
 * All static pages
 */
var db = require('../utils/db.js');

var isEmailValid = function(email){
	var matches = email.match(/[\w-\.+]+@[\w-]+\.[\w\.]+/g);
	return (matches && matches[0] && matches[0] == email);
};

var landingPage = function(req, res){
	// Cache current tags list rather than read from db for faster index page loads
	// Replace later with proper view caching
	/*
	db._query('SELECT * FROM tags',null,function(err, results){
		if(err)
			return res.render('index', {title: 'Curated HTML/CSS/JS UI Library'});

		return res.render('index', {title: 'Curated HTML/CSS/JS UI Library', tags: results.rows});
	});
	*/
	return res.render('index', {title: 'Curated HTML, CSS, JS UI Component Library'});
};

exports.route = function(app){
	// Home page
	app.get('/', landingPage);
	app.get('/index', landingPage);

	app.get('/demo', function(req, res){
		res.render('demo', {title: 'demo'});
	});

	// Notification signup
	app.post('/notify/signup', function(req, res){
		if(!req.body.email)
			return res.status(400).json({err: 'email required.'});

		if(!isEmailValid(req.body.email))
			return res.status(400).json({err: 'invalid email.'});

		db.insert('emails', ['email'], [req.body.email], function(err, results){
			if(!err || err.code == 23505)
				return res.status(200).json({ok: true});

			return res.status(500).json({err: 'Server error. Try again later'});
		});
	});
};