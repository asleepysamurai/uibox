/*
** Utility middleware
*/

exports.tags = function(req, res, next){
	var tags = [{"id":1,"name":"accordion"},{"id":2,"name":"collapsible"},{"id":3,"name":"jquery"},{"id":4,"name":"autocomplete"},{"id":5,"name":"typeahead"},{"id":6,"name":"jquery-ui"},{"id":7,"name":"backbone"},{"id":8,"name":"bootstrap"},{"id":9,"name":"button"},{"id":10,"name":"calendar"},{"id":11,"name":"date-picker"},{"id":12,"name":"foundation"},{"id":13,"name":"carousel"},{"id":14,"name":"angular-js"},{"id":15,"name":"color-picker"},{"id":16,"name":"drag-drop"},{"id":17,"name":"dropdown"},{"id":18,"name":"combo-box"},{"id":19,"name":"select"},{"id":20,"name":"file-picker"},{"id":21,"name":"gauge"},{"id":22,"name":"raphael"},{"id":23,"name":"dojo"},{"id":24,"name":"graph"},{"id":25,"name":"visualization"},{"id":26,"name":"infinite-scroll"},{"id":27,"name":"input"},{"id":28,"name":"input-mask"},{"id":29,"name":"input-tag"},{"id":30,"name":"knob"},{"id":31,"name":"lightbox"},{"id":32,"name":"zepto"},{"id":33,"name":"map"},{"id":34,"name":"menu"},{"id":35,"name":"modal-dialog"},{"id":36,"name":"mootools"},{"id":37,"name":"pagination"},{"id":38,"name":"progress"},{"id":39,"name":"scrollbar"},{"id":40,"name":"sidebar"},{"id":41,"name":"slider"},{"id":42,"name":"spinner"},{"id":43,"name":"stepper"},{"id":44,"name":"syntax"},{"id":45,"name":"tab"},{"id":46,"name":"toolbar"},{"id":47,"name":"tooltip"},{"id":48,"name":"tour"},{"id":49,"name":"tree"}];
	res.locals.tags = tags;
	next();
};