;
var fs = require('fs');

var _logger;

module.exports = {};

module.exports.Logger = function(options){
	if(_logger)
		return _logger;
	
	options = options || {};
	options.file = options.file || __dirname + '/logs/run.log';
	options.logLevel = options.logLevel || 'info';

	var logDir = options.file.substring(0, options.file.lastIndexOf('/'));
	try{
		var files = fs.readdirSync(logDir);
	} catch(err){
		if(err.errno == 34)
			fs.mkdirSync(logDir);
	}

	this.writeStream = fs.createWriteStream(options.file, {flags: 'a', encoding: 'utf8'});
	this.logLevel = options.logLevel;

	_logger = this;
	return this;
};
module.exports.Logger.prototype = {};

module.exports.Logger.prototype.log = function(level, message, _console){
	if(level && !message){
		message = level;
		level = null;
	}
	if(!level)
		level = 'info';

	level = level.toUpperCase();

	var levels = ['debug', 'info', 'warn', 'error'];

	var env = process.env.NODE_ENV || 'development';

	if(levels.indexOf(this.logLevel.toLowerCase()) <= levels.indexOf(level.toLowerCase())){
		message = '\n'+(new Date()).toISOString()+': '+level+': '+message+' ['+env+']';
		if(env == 'development' || _console)
			console.log(message);
		if(env != 'development')
			this.writeStream.write(message);
	}
};

module.exports.Logger.prototype.debug = function(message, console){
	this.log('debug', message, console);
};

module.exports.Logger.prototype.info = function(message, console){
	this.log('info', message, console);
};

module.exports.Logger.prototype.warn = function(message, console){
	this.log('warn', message, console);
};

module.exports.Logger.prototype.error = function(message, console){
	this.log('error', message, console);
};