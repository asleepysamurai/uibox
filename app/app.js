
/**
 * Server.
 */

var express = require('express')
  , http = require('http')
  , path = require('path');

var config = require('./config/app.js');

var logger = new (require('./utils/logger.js')).Logger();
var mware = require('./utils/middleware.js');

var routes = ['statics', 'search', 'item'];
/*
	accounts = require('./routes/accounts.js'),
	user = require('./routes/user.js'),
	api = require('./routes/api.js');
*/
var app;

var configExpress = function(){
	logger.info('Configuring Express Server...');
	app = express();
	
	// Disable sending X-Powered-By header
	app.disable('x-powered-by');

	// all environments
	app.set('port', config.express.port);
	app.set('views', config.express.viewDir);
	app.set('view engine', 'ejs');

	app.use(function(req, res, next){ res.set(config.headers); next(); }); // Mess around with people inspecting headers :P
	app.use(express.bodyParser());
	app.use(express.cookieParser());
	app.use(express.cookieSession(config.session));

	// Do not check csrf for api endpoints, authenticate instead
	app.use(function(req, res, next){ 
		if(req.path.indexOf('/api') == 0)
			api.auth(req, res, next);
		else
			express.csrf()(req, res, next);
	});
	app.use(function(req, res, next){
		// Make user csrf token available to templates
		res.locals.csrf = req.session ? req.session._csrf : null;

		// Monkeypatch res.locals object
		res.locals.put = function(o){
			for(var i=1;i<arguments.length;++i)
				o[arguments[i]] = arguments[++i];
			return o;
		};

		// Make user data available to templates
		if(req.session.uid){
			res.locals.user = {
				id: req.session ? req.session.uid : null,
				email: req.session ? req.session.email : null
			};
		}

		// Make environment available to templates
		res.locals.environment = app.get('env');

		next();
	});

	app.use(mware.tags);

	app.use(app.router);

	// development only
	if ('development' == app.get('env')) {
		app.use(express.errorHandler());
	}
};

var setupRoutes = function(){
	logger.info('Setting up routes...');
	for(var i=0;i<routes.length;++i)
		(require('./routes/'+routes[i]+'.js')).route(app);
	/*
	statics.route(app);
	accounts.route(app);
	user.route(app);
	api.route(app);
	*/
};

var listen = function(){
	logger.info('Setup complete... Starting app...');
	http.createServer(app).listen(app.get('port'), function(){
		logger.info('Express server listening on port ' + app.get('port'));
		logger.info('Launch sequence complete. All systems go. App started.', true);
	});
};

module.exports = {};

module.exports.start = function(){
	configExpress();
	setupRoutes();
	listen();
};