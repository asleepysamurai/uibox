$(window).ready(function(){
	var $nt = $('#nav-toggler');
	$nt.parent().on('click', function(){
		var togglerClasses = ['icon-arrow-down', 'icon-arrow-up'];
		if($nt.hasClass(togglerClasses[0]))
			$nt.removeClass(togglerClasses[0]).addClass(togglerClasses[1]);
		else
			$nt.removeClass(togglerClasses[1]).addClass(togglerClasses[0]);
	});
	$('#nav-btn-tags').on('click', function(){
		var tc = function(selector){
			$(selector).toggleClass('active');
		};
		tc('#page-header-tags');
		tc(this);
		tc('div .fullpage');
	});
	$('div .fullpage').on('click', function(){
		$('#nav-btn-tags').trigger('click');
	});
});
