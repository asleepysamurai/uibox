$(window).ready(function(){
	$('#close-signup').on('click', function(){
		$('#notification-signup').css('min-height', '0px').slideUp();
	});
	if(!localStorage.getItem('notications.signedup'))
		$('#notification-signup').css('min-height', '0px').slideDown();

	$('#in-email').tooltip({placement: 'bottom', trigger: 'manual', html: true, title: 'Hit enter to submit.'}).on('focus', function(){
		$(this).tooltip('show');
	}).on('focusout', function(){
		$(this).tooltip('hide');
	});

	var showError = function(content){
		$('#in-email')
			.tooltip('destroy')
			.tooltip({placement: 'bottom', trigger: 'manual', html: true, title: content})
			.tooltip('show')
			.on('focusout', function(){
				$(this).tooltip('hide');
			});
	};

	var isEmailValid = function(email){
		var matches = email.match(/[\w-\.+]+@[\w-]+\.[\w\.]+/g);
		return (matches && matches[0] && matches[0] == email);
	};

	$('#notification-signup form').on('submit', function(ev){
		ev.preventDefault();

		var email = $('#in-email').val();
		if(email.length == 0)
			return showError('Email cannot be empty.');
		if(!isEmailValid(email))
			return showError('Email id looks invalid.');

		$.ajax({
			url: '/notify/signup',
			data: {_csrf: $('#in-csrf').val(), email: email},
			dataType: 'json',
			type: 'POST',
			statusCode: {
				400: function(xhr, textStatus, error){
					showError(xhr.responseText.indexOf('required') > -1 ? 'Email cannot be empty.' : 'Email id looks invalid.');
				},
				500: function(xhr, textStatus, error){
					showError('Something went wrong while trying to sign you up. Please try again later.');
				},
				200: function(data, textStatus, xhr){
					$('#notification-signup > .container > span').text('Thanks for signing up! We\'ll send you an email once a week or so with progress updates.');
					localStorage.setItem('notications.signedup', true);
				}
			}
		});
	});
});