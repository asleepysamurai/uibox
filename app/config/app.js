
/*
 * Application Configuration
 */

var fs = require('fs');
var extend = require('../utils/extend');

// Dev config
var env = process.env.NODE_ENV || 'development';

// Common config settings
var appRoot = fs.realpathSync(__dirname+'/../');
var config = {
	express: {
		port: 9975,
		viewDir: appRoot+'/views'
	},
	logger: {
		file: appRoot+'/logs/run.log',
		level: 'info'
	},
	nginx: {
		basedir: appRoot+'/public',
		configFile: appRoot+'/config/nginx.conf'
	},
	minify: {
		js: { merge: [], split: [] },
		css: { merge: [], split: [] },
	},
	session: {
		key: 'PHPSESSID',
	},
	db: {
		name: 'uidb',
		host: 'localhost'
	}
};

if('development' == env){
	var env_config = {
		session: {
			secret: 'b380a99a06512bdc714dae589d5753c8f9341545c563f723c1fd160f290abf07'
		},
		db: {
			user: 'uiapi_dev',
			pass: 'qwaszxwesdxc',
		}
	};
}
else if('production' == env){
	var env_config = {
		minify: {
			js: { merge: ['jquery.1.10.2','bootstrap', 'notification' ,'app'] },
			css: { merge: ['bootstrap','icomoon','app'], split: [] },
		},
		session: {
			secret: 'b4d8df689af69ce77ecbf8271296180712c4edf0df2ffddc29ae3f6d8593f0ff'
		},
		db: {
			user: 'uiapi_prod',
			pass: 'qwaszxwesdxc',
		}
	};
}

module.exports = extend(true, config, env_config);